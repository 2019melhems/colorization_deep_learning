This repo contains a notebook for our model, and a second one for testing the official implementation of the paper Colorful Image Colorization https://arxiv.org/pdf/1603.08511.pdf on our test dataset.  
This is the result of a small set of Cifar-10 dataset.  
![result](re-colored_images.PNG)  
The loss obtained by training by modifying the optimizer can be summarized:  
For Adam optimizer with 1e-3 learning rate the loss is 181  
For Adam optimizer with 5e-3 learning rate the loss is 192  
For SGD optimizer with 1e-2 learning rate the loss is 189  
For SGD optimizer with 5e-2 learning rate the loss is 190  